package Koha::Plugin::Cz::Knihovny::AvailabilityApi::BibliosController;

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;

use Mojo::Base 'Mojolicious::Controller';

use Koha::Biblios;

=head1 Koha::Plugin::Cz::Knihovny::AvailabilityApi::BibliosController

A class implementing the controller methods for getting biblio

=head2 Class Methods

=head3 get_biblio

Method to get hold availability of biblio

=cut

sub get_biblio {
    my $c = shift->openapi->valid_input or return;

    my $biblio_id = $c->validation->param('biblio_id');
    my $biblio    = Koha::Biblios->find($biblio_id);
    return $c->render( status => 404, openapi => { error => "Object not found." } )
        unless $biblio;

    return $c->render( status => 200, openapi => $biblio->unblessed );
}

=head2 Internal methods

=cut

1;
